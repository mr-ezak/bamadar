import React, { useEffect, useState, useCallback } from "react";
import styled from "styled-components";
import axios from "axios";
import Router from "next/router";
import { config, loginCheck } from "../utils/main";
import { FinalizeOrderItem } from "../components/FinalizeOrderItem";
import { Price } from "../components/shared/Price";
import { Space } from "../components/shared/Space";
import { RecieptIcon } from "../components/icons/Icons";

export default () => {
  const [cart, setCart] = useState([]);
  const [userOrder, setUserOrder] = useState([]);
  const [cartDetails, setCartDetails] = useState({
    sum: 0,
    delivery: 0,
    finalSum: 0,
  });

  useEffect(() => {
    inMadarPack();
  }, []);

  const inMadarPack = useCallback(async () => {
    const mad = await window.localStorage.getItem("MadarPack");
    if (mad) {
      const packLocalStorage = JSON.parse(mad);
      setUserOrder(packLocalStorage.pack);
    }
  }, [setUserOrder]);

  const initialCartList = useCallback(async () => {
    if (window.localStorage.getItem("card")) {
      const cartLocalStorage = JSON.parse(window.localStorage.getItem("card"));
      setCart(cartLocalStorage);
    }
  }, [setCart]);

  const FinalizeMyOrder = async () => {
    const res = await axios.post(config + "/api/order-submit", {
      package: JSON.stringify(userOrder),
      headers: {
        Authorization: `bearer${window.localStorage.getItem("access_token")}`,
      },
    });

    // console.log(res.data);
    if (res.data.message == "ok") {
      // console.log("ok");
      if (userOrder.user.payment == "cash") {
        Router.push("/cashSuccess", "success");
      } else {
        // Router.push(res.data.url);
        window.location = res.data.url;
        // console.log(res.data.url);
      }
    } else {
      if (userOrder.user.payment == "cash") {
        Router.push("/unuccess");
      }
    }
  };

  useEffect(() => {
    initialCartList();
  }, [initialCartList]);

  //Calculate card on update
  useEffect(() => {
    let sum = 0;

    cart.forEach((item) => {
      sum += item.price * item.balance;
    });

    // console.log(userOrder);

    if (userOrder && userOrder.user && userOrder.user.city == "dezful") {
      if (cartDetails.sum >= 1500000) {
        setCartDetails({
          sum,
          delivery: 0,
          finalSum: sum + 0,
        });
      } else if (cartDetails.sum < 1500000) {
        setCartDetails({
          sum,
          delivery: 50000,
          finalSum: sum + 50000,
        });
      }
    } else {
      console.log(userOrder);
      if (cartDetails.sum >= 3000000) {
        setCartDetails({
          sum,
          delivery: 0,
          finalSum: sum + 0,
        });
      } else if (cartDetails.sum < 3000000) {
        setCartDetails({
          sum,
          delivery: 100000,
          finalSum: sum + 100000,
        });
      }
    }
  }, [cart, setCartDetails, userOrder]);

  return (
    <Wrapper>
      <Title>فاکتور نهایی</Title>
      <Logo>
        <RecieptIcon />
      </Logo>
      <ButtonWrapper
        onClick={() => {
          Router.push("/cart");
        }}>
        <span>ویرایش سبد خرید</span>
      </ButtonWrapper>
      {cart.length != 0 &&
        cart.map((item) => (
          <FinalizeOrderItem
            key={item.id}
            name={item.name}
            balance={item.balance}
            measurement={item.measurement}
            finalPrice={(item.price * item.balance) / 10}
          />
        ))}

      {cart.length == 0 && <div>سبد خرید خالی است</div>}

      <Space vertical={20} />

      <CardDetails>
        <Title>[جزییات]</Title>
        <RowData>
          <Field>مجموع سبد: </Field>
          <Value>
            <Price>{cartDetails.sum}</Price>
          </Value>
        </RowData>
        <RowData>
          <Field>هزینه حمل: </Field>
          <Value>
            <Price>{cartDetails.delivery}</Price>
          </Value>
        </RowData>
        <Space vertical={10} />
        <hr />
        <Space vertical={10} />

        <RowData>
          <Field>مجموع فاکتور: </Field>
          <Value>
            <Price>{cartDetails.finalSum}</Price>
          </Value>
        </RowData>

        <Space vertical={20} />
        <hr />
        <Space vertical={10} />

        <RowData>
          <Field>نام و نام خانوادگی: </Field>
          <Value>
            {userOrder && userOrder.user && userOrder.user.family}
            {" - "}
            {userOrder && userOrder.user && userOrder.user.name}
          </Value>
        </RowData>

        <RowData>
          <Field>شماره تماس: </Field>
          <Value>{userOrder && userOrder.user && userOrder.user.tel}</Value>
        </RowData>

        <RowData>
          <Field>آدرس: </Field>
          <Value>
            {userOrder && userOrder.user && userOrder.user.city}
            {" - "}
            {userOrder &&
              userOrder.user &&
              userOrder.user.address.split("&")[0]}
            {" - "}
            {"پلاک " + userOrder && userOrder.user && userOrder.user.pelak}
          </Value>
        </RowData>

        <RowData>
          <Field>توضیحات: </Field>
          <Value>{userOrder && userOrder.user && userOrder.user.des}</Value>
        </RowData>
      </CardDetails>
      <ButtonWrapper
        onClick={() => {
          Router.push("/cart");
        }}>
        <span>ویرایش سبد خرید</span>
      </ButtonWrapper>
      <Space vertical={50} />
      <FixDown
        onClick={() => {
          if (loginCheck() == "0") {
            // setPage("login");
          } else {
            FinalizeMyOrder();
          }
        }}>
        <Title>تکمیل فرایند</Title>
      </FixDown>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  position: absolute;
  width: 100%;
  min-height: 100%;
  height: auto;
  background: #fff;
  color: #000;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Title = styled.h2`
  text-align: center;
`;
const Logo = styled.div`
  width: 130px;
  height: 130px;
  border-radius: 65px;
  margin: 0px auto;
  outline: 1px dashed #ccc;
  outline-offset: -10px;
  -moz-outline-radius: 15px;
  overflow: hidden;
  svg {
    margin-left: 20px;
    margin-top: 20px;
    display: block;
  }
`;
const CardDetails = styled.div`
  width: 80%;
  min-width: 350px;
  /* margin: 40px; */
  border: 1px solid #ccc;
  background: #eee;
  border-radius: 15px;
  padding: 10px 20px;
`;

const RowData = styled.div`
  display: flex;
  justify-content: space-between;
  direction: rtl;
  text-align: right;
`;

const Field = styled.div`
  font-size: 18px;
  font-weight: 300;
`;

const Value = styled.div`
  font-size: 20px;
  direction: rtl;
`;
const ButtonWrapper = styled.div`
  width: 150px;
  height: 35px;
  background: #3498db;
  border-radius: 5px;
  padding: 5px;
  outline: 1px dashed #fff;
  outline-offset: -5px;
  -moz-outline-radius: 5px;
  text-align: center;
  color: #fff;
  margin-top: 30px;
  margin-bottom: 30px;
  cursor: pointer;
`;
const FixDown = styled.a`
  width: 100%;
  height: 50px;
  text-decoration: none;

  position: fixed;
  bottom: 0;
  left: 0;
  background: #ff4177;
  color: #fff;

  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  box-shadow: 0 0 3px #333;
  transition: all 200ms;

  :hover {
    background: #994177;
  }
`;
