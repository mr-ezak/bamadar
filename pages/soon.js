import React from "react";
import styled from "styled-components";

export default () => {
  return (
    <Wrapper>
      <>
        <Title>
          <img src='../images/Soon.png' />
        </Title>
        {/* <Button onClick={() => setPage("home")}>خانه</Button> */}
        <Text>به زودی با شما خواهیم بود</Text>
      </>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  direction: rtl;
`;

const Title = styled.h2`
  font-size: 16px;
  font-weight: 400;
  color: #333;
  text-align: center;
  img {
    display: block;
    width: 200px;
    height: 220px;
    margin: 0px auto;
  }
`;
const Text = styled.div`
  width: 300px;
  height: 50px;
  line-height: 50px;
  color: #b05082;
  font-weight: bold;
  margin: 20px auto;
  font-size: 18px;
  text-align: center;
`;
const Button = styled.div`
  width: 150px;
  text-align: center;
  display: block;
  padding: 8px 10px;
  margin: 10px auto;
  border-radius: 5px;
  background: #3498db;
  color: #efefef;
  cursor: pointer;
`;
