import React from "react";
import styled from "styled-components";
import Router from "next/router";
import { TickIcon } from "../components/icons/Icons";

export default function PayStatus() {
  return (
    <Wrapper>
      <>
        <LogoCon>
          <TickIcon />
        </LogoCon>
        <Title>سفارش شما با موفقیت تایید شد</Title>
        <Button onClick={() => Router.push("/")}>لیست سفارشات</Button>
      </>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  direction: rtl;
`;
const LogoCon = styled.div`
  width: 100px;
  height: 100px;
  border-radius: 15px;
  outline: 1px dashed #27ae60;
  outline-offset: -5px;
  -moz-outline-radius: 15px;
  overflow: hidden;
  margin: 40px auto;
  margin-bottom: 50px;
  background: #fff;
  svg {
    width: 50px;
    height: 50px;
    margin: 25px auto;
    display: block;
  }
`;
const Title = styled.h2`
  font-size: 16px;
  font-weight: 400;
  color: #fff;
  text-align: center;
  margin-bottom: 50px;
`;

const Button = styled.div`
  width: 150px;
  text-align: center;
  display: block;
  padding: 8px 10px;
  margin: 10px auto;
  border-radius: 5px;
  background: #3498db;
  color: #efefef;
  cursor: pointer;
  font-size: 13px;
`;
