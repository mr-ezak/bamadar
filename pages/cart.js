import React, { useEffect, useState, useCallback, useContext } from "react";
import styled from "styled-components";
import Router from "next/router";
import { Header } from "../components/Header";
import { CardItem } from "../components/CardItem";
import { Price } from "../components/shared/Price";
import { Space } from "../components/shared/Space";
import { BasketIcon } from "../components/icons/Icons";
import { MainWrapper, ContentWrapper } from ".";
import { CardContext } from "./_app";

export default () => {
  const [display, setDisplay] = useState("none");
  const [cartDetails, setCartDetails] = useState({
    sum: 0,
    finalSum: 0,
  });

  const {
    items,
    addToCard,
    increaseCardBalance,
    decreaseCardBalance,
  } = useContext(CardContext);

  useEffect(() => {
    if (window.location.href != "localhost") {
      setDisplay("block");
    }
  }, []);

  //   Calculate card on update
  useEffect(() => {
    let sum = 0;

    items.forEach((item) => {
      sum += item.price * item.balance;
    });
    setCartDetails({
      sum,
      finalSum: sum,
    });
  }, [items, setCartDetails]);

  // add to basket

  return (
    <MainWrapper>
      <Header display={display} setDisplay={setDisplay} />

      <ContentWrapper boxed>
        <Title>سبد خرید</Title>
        <Logo>
          <BasketIcon />
        </Logo>
        {items.length != 0 &&
          items.map((item) => (
            <CardItem
              key={item.id}
              name={item.name}
              balance={item.balance}
              finalPrice={item.price * item.balance}
              measurement={item.measurement}
              add={() => increaseCardBalance(item)}
              remove={() => decreaseCardBalance(item)}
            />
          ))}

        {items.length == 0 && <Title>سبد خرید خالی است</Title>}

        <Space vertical={20} />

        <CardDetails>
          <Title>مجموع کل</Title>
          <RowData>
            <Field>مجموع سبد: </Field>
            <Value>
              <Price>{cartDetails.sum}</Price>
            </Value>
          </RowData>
        </CardDetails>
        <Space vertical={50} />
        <FixDown
          onClick={() => {
            Router.push("/order");
          }}
        >
          <Title>تایید سبد خرید</Title>
        </FixDown>
      </ContentWrapper>
    </MainWrapper>
  );
};

const Title = styled.h2`
  width: 90% !important;
  height: 50px;
  margin: 10px auto;
  border-bottom: 1px dashed #333;
  text-align: center;
`;
const Logo = styled.div`
  width: 130px;
  height: 130px;
  border-radius: 65px;
  margin: 0px auto;
  outline: 1px dashed #ccc;
  outline-offset: -10px;
  -moz-outline-radius: 15px;
  overflow: hidden;
  svg {
    margin-left: 20px;
    margin-top: 20px;
    display: block;
  }
`;
const CardDetails = styled.div`
  width: 60%;
  min-width: 350px;
  margin: 0px auto;
  margin-bottom: 30px;
  border: 1px solid #ccc;
  background: #eee;
  border-radius: 15px;
  padding: 10px 20px;
  border: 1px dashed #333;
`;

const RowData = styled.div`
  display: flex;
  justify-content: space-between;
  direction: rtl;
  text-align: right;
`;

const Field = styled.div`
  font-size: 18px;
  font-weight: 300;
`;

const Value = styled.div`
  font-size: 20px;
`;

const FixDown = styled.a`
  width: 100%;
  height: 50px;
  text-decoration: none;

  position: fixed;
  bottom: 0;
  left: 0;
  background: #ff4177;
  color: #fff;

  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  box-shadow: 0 0 3px #333;
  transition: all 200ms;

  :hover {
    background: #994177;
  }
`;
