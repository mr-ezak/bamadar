import App from "next/app";
import Head from "next/head";
import React, { createContext, useEffect, useState } from "react";
import styled, {
  ThemeProvider,
  createGlobalStyle,
  css,
} from "styled-components";
import "../public/dana.css";
import "react-multi-carousel/lib/styles.css";
import { mobile } from "../utils/media";

const theme = {
  colors: {
    primary: "#0070f3",
  },
};

export const CardContext = React.createContext({
  addToCard: () => {},
  increaseCardBalance: () => {},
  decreaseCardBalance: () => {},
  items: [],
});
const NO_LOCAL_STORAGE = typeof window === "undefined" || !localStorage;

export default (props) => {
  const { Component, pageProps } = props;
  // state
  const [didHydrate, setDidHydrate] = useState(false);
  const [items, setItems] = useState([]);

  // effect 1 - hydrate
  useEffect(() => {
    if (NO_LOCAL_STORAGE) return;

    if (didHydrate) {
      return;
    }
    const cart = JSON.parse(window.localStorage.getItem("card"));
    if (cart) {
      setItems(cart);
    } else {
      setItems([]);
    }
    setDidHydrate(true);
  }, [didHydrate]);

  // effect 2 - on change, save to local storage
  // if (!hydrated) return // wait for initial filling to happen
  useEffect(() => {
    if (NO_LOCAL_STORAGE) return;

    if (!didHydrate) {
      return;
    }
    window.localStorage.setItem("card", JSON.stringify(items));
  }, [didHydrate, items]);

  const addToCard = (product) => {
    // console.log({ product });
    let card = [];
    let addNew = true;

    if (items) {
      card = items;
      card.forEach((item, i) => {
        if (product.id == item.id) {
          card[i].balance = item.balance + 1;
          addNew = false;
        }
      });
    }

    if (addNew) {
      card.push({
        id: product.id,
        balance: 1,
        name: product.name,
        price: product.price,
        measurement: product.measurement,
      });
    }
    setItems([...card]);
  };

  const increaseCardBalance = (product) => {
    let tempCard = [...items];

    tempCard.forEach((item, i) => {
      if (product.id == item.id) {
        switch (item.measurement) {
          case "کیلوگرم":
            tempCard[i].balance = (parseFloat(item.balance) + 0.5).toFixed(1);
            break;
          case "گرم":
            tempCard[i].balance = (parseFloat(item.balance) + 0.1).toFixed(1);
            break;

          default:
            tempCard[i].balance = item.balance + 1;
            break;
        }
      }
    });

    setItems(tempCard);
  };

  // add to basket
  const decreaseCardBalance = (product) => {
    let tempCard = [...items];
    let shouldRemoveId = false;

    tempCard.forEach((item, i) => {
      if (product.id == item.id) {
        if (item.balance > 1) {
          switch (item.measurement) {
            case "کیلوگرم":
              tempCard[i].balance = (parseFloat(item.balance) - 0.5).toFixed(1);
              break;
            case "گرم":
              tempCard[i].balance = (parseFloat(item.balance) - 0.1).toFixed(1);
              break;

            default:
              tempCard[i].balance = item.balance - 1;
              break;
          }
        } else {
          switch (item.measurement) {
            case "کیلوگرم":
              if (tempCard[i].balance <= 0.5) shouldRemoveId = item.id;
              else
                tempCard[i].balance = (parseFloat(item.balance) - 0.5).toFixed(
                  1
                );
              break;
            case "گرم":
              if (tempCard[i].balance <= 0.1) shouldRemoveId = item.id;
              else
                tempCard[i].balance = (parseFloat(item.balance) - 0.1).toFixed(
                  1
                );

              break;
            default:
              shouldRemoveId = item.id;
              break;
          }
        }
      }
    });

    if (shouldRemoveId !== false) {
      tempCard = tempCard.filter((item) => {
        return !(item.id === shouldRemoveId);
      });
    }

    setItems(tempCard);
  };

  return (
    <ThemeProvider theme={theme}>
      <CardContext.Provider
        value={{ items, addToCard, increaseCardBalance, decreaseCardBalance }}
      >
        <Head>
          <title>اپلیکیشن بامادر</title>
        </Head>
        <GlobalStyle />
        <Component {...pageProps} />
      </CardContext.Provider>
    </ThemeProvider>
  );
};

const GlobalStyle = createGlobalStyle`
  *{
    padding:0;
    margin: 0;
    box-sizing: border-box;
  }
  
  body {
    background: #fff;
    color: #fff;
    font-family: dana  !important;
  }
`;
// const BoxedWrapper = styled.div`
//   margin: 0 auto;
//   width: auto;
//   max-width: 1000px;
//   overflow: hidden;

//   ${mobile(css`
//     width: 100%;
//   `)}
// `
