import React, { useState, useEffect } from "react";
import styled, { css } from "styled-components";
//Importing Pages
import { Header } from "../components/Header";
// import { Products } from "../components/Products";
import { Home } from "../components/Home";
import { Dummy } from "../components/Dummy";

//Main Page Component
export default () => {
  const [apiAdd, setApiAdd] = useState("https://my.bamadar.com");
  const [searchInput, setSearchInput] = useState("");
  const [card, setCard] = useState([]);
  const [msgBox, setMsgBox] = useState({
    msgstatus: "-50px",
  });

  const [page, setPage] = useState("home");
  const [goBack, setGoBack] = useState("none");

  useEffect(() => {
    setTimeout(() => {
      setMsgBox({
        msgstatus: "-50px",
      });
    }, 2000);
  }, [msgBox, setMsgBox]);

  return (
    <MainWrapper>
      <Header
        onChange={setSearchInput}
        cardList={card}
        display={goBack}
        setPage={setPage}
      />
      <ContentWrapper>
        {page == "home" && (
          <Home msgBox={msgBox} setMsgBox={setMsgBox} apiAdd={apiAdd} />
        )}

        <Boxed>
          {page == "products" && searchInput != "" && (
            <Products
              apiAdd={apiAdd}
              name={searchInput}
              setCard={setCard}
              page={page}
            />
          )}

          {page == "dummy" && <Dummy setPage={setPage} />}
        </Boxed>
      </ContentWrapper>
      <MsgBox msgstatus={msgBox.msgstatus}>
        <p>با موفقیت به سبد خرید اضافه شد</p>
      </MsgBox>
    </MainWrapper>
  );
};

export const MainWrapper = styled.div`
  margin: 0 auto;
  width: 100%;
  max-width: 1000px;
  height: 100vh;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  align-items: center;
  font-family: Dana;
`;

export const ContentWrapper = styled.div`
  flex-grow: 1;
  width: 100%;
  background: #ffffff;
  border-radius: 16px 16px 0px 0px;
  overflow: auto;
  margin-top: -20px;
  color: #000;

  ${(p) =>
    p.boxed &&
    css`
      padding: 20px;
    `}
`;

const Boxed = styled.div`
  padding: 20px;
`;
const MsgBox = styled.div`
  position: fixed;
  width: 250px;
  height: 40px;
  line-height: 40px;
  background: #2ecc71;
  bottom: ${(d) => (d.msgstatus ? d.msgstatus : "-50px")};
  margin: 0px auto;
  text-align: center;
  float: right;
  color: #fff;
  border-radius: 5px;
  transition: all 500ms;
`;

// color pallet https://colorhunt.co/palette/175777
