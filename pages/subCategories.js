import React, { useState, useEffect, useCallback } from "react";
import styled, { css } from "styled-components";
import Router, { useRouter } from "next/router";
import axios from "axios";
import { getQueryVariable } from "../utils/main";
import { Loading } from "../components/Loading";
import { SmallBasketIcon } from "../components/icons/Icons";
import { config } from "../utils/main";
import { MainWrapper, ContentWrapper } from ".";
import Head from "next/head";
import { Header } from "../components/Header";

export default () => {
  const [subCats, setSubCats] = useState([]);
  const { query, push } = useRouter();
  const subCatId = query.id ? query.id : undefined;

  const getSubCat = async (id) => {
    const response = await axios.get(config + "/api/categories?sub=" + id);
    const arr = response.data.data;
    const newSubCat = [];
    arr.map((data) => {
      if (data.parent_id == id) {
        newSubCat.push(data);
      }
    });
    setSubCats(newSubCat);
  };

  useEffect(() => {
    if (subCatId) {
      getSubCat(subCatId);
    }
  }, [subCatId]);

  return (
    <MainWrapper>
      <Head>
        <title>جستجو :: بامادر</title>
      </Head>
      <Header display={"none"} />
      <ContentWrapper>
        <Title>زیر دسته ها</Title>
        <SubCatsCon>
          {subCats.length != 0 ? (
            subCats.map((data, index) => (
              <Item
                key={index}
                onClick={() => {
                  //   getPros(data.id);
                  Router.push("/products?action=" + data.id);
                }}
              >
                <SmallBasketIcon />
                <span>{data.name}</span>
              </Item>
            ))
          ) : (
            // <NotFound>نتیجه ای یافت نشد</NotFound>
            <Loading />
          )}
        </SubCatsCon>
        <GoBackBTN
          onClick={() => {
            Router.push("/");
          }}
        >
          بازگشت
        </GoBackBTN>
      </ContentWrapper>
    </MainWrapper>
  );
};

const Wrapper = styled.div`
  direction: rtl;
  background: #fff;
  position: absolute;
  width: 100%;
  height: 100%;
`;
const NotFound = styled.span`
  display: block;
  width: 100%;
  height: 40px;
  text-align: center;
  color: #666;
  margin-top: 20px;
`;
const Title = styled.h2`
  width: 80%;
  height: 40px;
  border-bottom: 1px dashed #333;
  margin: 10px auto;
  font-size: 16px;
  font-weight: 400;
  color: #333;
  text-align: center;
`;
const SubCatsCon = styled.div`
  width: 100%;
  height: auto;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`;
const Item = styled.div`
  width: 125px;
  height: 125px;
  background: #ccc;
  border-radius: 5px;
  background: #b05082;
  margin-top: 10px;
  margin-right: 10px;
  cursor: pointer;
  outline: 1px dashed #efefef;
  outline-offset: -8px;
  -moz-outline-radius: 5px;
  padding: 10px;
  transition: all 300ms;
  color: #fff;
  svg {
    display: block;
    width: 50px;
    height: 50px;
    margin: 5px auto;
    path {
      text-align: center;
    }
  }
  span {
    display: block;
    font-size: 12px;
    text-align: center;
    margin-top: 5px;
  }
  :hover {
    background: #7b385b;
  }
`;
const GoBackBTN = styled.div`
  width: 100px;
  height: 40px;
  line-height: 40px;
  background: #3498db;
  margin: 20px auto;
  font-size: 11px;
  border-radius: 5px;
  text-align: center;
  outline: 1px dashed #efefef;
  outline-offset: -5px;
  -moz-outline-radius: 5px;
  cursor: pointer;
`;
