import React, { useEffect, useState, useCallback } from "react";
import styled from "styled-components";
import { Formik, Form, Field, ErrorMessage } from "formik";
import Router from "next/router";
import { loginCheck } from "../utils/main";
import { Login } from "../components/Login";
import { Header } from "../components/Header";

import { Space } from "../components/shared/Space";
import { MainWrapper, ContentWrapper } from ".";

export default () => {
  const [display, setDisplay] = useState("none");
  const [cart, setCart] = useState([]);
  const [logged, setLogged] = useState("");
  const [maMobile, setMaMobile] = useState(undefined);

  const [order, setOrder] = useState({
    name: undefined,
    family: undefined,
    tel: undefined,
    address: undefined,
    pelak: undefined,
    des: undefined,
  });

  useEffect(() => {
    if (loginCheck() != 1) {
      setLogged("0");
    } else {
      setLogged("1");
    }

    if (window.localStorage.getItem("MadarCell")) {
      setMaMobile(window.localStorage.getItem("MadarCell"));
    }

    if (window.localStorage.getItem("MadarOrder")) {
      let MadarArr = JSON.parse(window.localStorage.getItem("MadarOrder"));
      setOrder({
        name: MadarArr.values.name,
        family: MadarArr.values.family,
        tel: MadarArr.values.tel,
        address: MadarArr.values.address,
        pelak: MadarArr.values.pelak,
        des: MadarArr.values.des,
      });
      //   setName(MadarArr.values.name);
      //   setTel(MadarArr.values.tel);
      //   setAddress(MadarArr.values.address);
      //   setPelak(MadarArr.values.pelak);
    }

    initialCardList();
  }, []);

  const initialCardList = () => {
    if (window.localStorage.getItem("card")) {
      const cartLocalStorage = JSON.parse(window.localStorage.getItem("card"));
      setCart(cartLocalStorage);
    }
  };

  if (logged == 0) {
    return (
      <MainWrapper>
        <Header display={display} />
        <ContentWrapper boxed>
          <Login setLogged={setLogged} />
        </ContentWrapper>
      </MainWrapper>
    );
  }

  return (
    <MainWrapper>
      <Header display={display} />
      <ContentWrapper boxed>
        <Title>مشخصات کاربر</Title>
        <Space vertical={30} />
        <Formik
          initialValues={{
            name: order.name && order.name,
            family: order.family && order.family,
            tel: order.tel && order.tel,
            mobile: maMobile && maMobile,
            city: "dezful",
            address: order.address && order.address,
            pelak: order.pelak && order.pelak,
            des: order.des && order.des,
            payment: "cart",
          }}
          validate={(values) => {
            const errors = {};
            if (!values.name) {
              errors.name = "نام خود رو وارد کنید";
            } else if (!values.family) {
              errors.family = "نام خانوادگی خود را وارد کنید";
            } else if (!values.tel) {
              errors.tel = "تلفن خود رو وارد کنید";
            }
            //  else if (!values.mobile) {
            //   errors.mobile = "همراه خود را وارد کنید";
            // }
            else if (!values.city) {
              errors.city = "شهر خود را انتخاب کنید";
            } else if (!values.address) {
              errors.address = "آدرس الزامیست ";
            } else if (!values.pelak) {
              errors.pelak = "پلاک خود رو وارد کنید";
            } else if (!values.payment) {
              errors.pelak = "روش پرداخت خود رو انتخاب کنید";
            }
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            setTimeout(() => {
              if (loginCheck() != "0") {
                const sendPack = async () => {
                  if (window.localStorage.getItem("MadarOrder")) {
                    window.localStorage.setItem(
                      "MadarOrder",
                      JSON.stringify({ values })
                    );
                  }

                  let pack = {
                    user: values,
                    card: cart,
                  };

                  let newPack = pack.user.address.concat("&" + pack.user.city);
                  pack.user.address = newPack;

                  window.localStorage.setItem(
                    "MadarPack",
                    JSON.stringify({ pack })
                  );
                };
                sendPack();
                Router.push("/finalizeOrder");
              } else {
                // console.log("Go login");
              }

              setSubmitting(false);
            }, 400);
          }}>
          {({ isSubmitting }) => (
            <StyledForm>
              <RowData>
                <Label> نام </Label>
                <Value>
                  <StyledField type='text' name='name' />
                  <ErrorMessage name='name' component='div' />
                </Value>
              </RowData>

              <RowData>
                <Label> نام خانوادگی </Label>
                <Value>
                  <StyledField type='text' name='family' />
                  <ErrorMessage name='family' component='div' />
                </Value>
              </RowData>

              <RowData>
                <Label>تلفن ثابت</Label>
                <Value>
                  <StyledField type='text' name='tel' />
                  <ErrorMessage name='tel' component='div' />
                </Value>
              </RowData>

              <RowData>
                {/* <Label>تلفن همراه</Label> */}
                <Value>
                  <StyledField type='hidden' name='mobile' />
                  {/* <ErrorMessage name='mobile' component='div' /> */}
                </Value>
              </RowData>

              <RowData>
                <Label htmlFor='location'>شهر</Label>
                <Value>
                  <Field component='select' name='city'>
                    <option value='dezful' selected>
                      دزفول
                    </option>
                    <option value='andimeshk'>اندیمشک</option>
                  </Field>
                </Value>
              </RowData>

              <RowData>
                <Label>آدرس </Label>
                <Value>
                  <StyledField type='text' name='address' />
                  <ErrorMessage name='address' component='div' />
                </Value>
              </RowData>

              <RowData>
                <Label>پلاک </Label>
                <Value>
                  <StyledField type='text' name='pelak' />
                  <ErrorMessage name='pelak' component='div' />
                </Value>
              </RowData>

              <RowData>
                <Label name='des'>توضیحات </Label>
                <Value>
                  <StyledField type='text' name='des' />
                  <ErrorMessage name='des' component='div' />
                </Value>
              </RowData>

              <RowData>
                <Label>نحوه پرداخت </Label>
                <Value>
                  <Field component='select' name='payment'>
                    <option value='card' selected>
                      درگاه بانکی
                    </option>
                    <option value='cash'>پرداخت نقدی</option>
                  </Field>
                </Value>
              </RowData>

              <FixDown type='submit' disabled={isSubmitting}>
                <Title>ثبت اطلاعات</Title>
              </FixDown>
            </StyledForm>
          )}
        </Formik>
      </ContentWrapper>
    </MainWrapper>
  );
};

const BigWrapper = styled.div`
  width: 100%;
  position: absolute;
  min-height: 100%;
  height: auto;
  background: #fff;
`;
const Wrapper = styled.div`
  width: 100%;
  background: #fff;
  color: #000;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  form {
    margin-bottom: 100px;
  }
`;

const Title = styled.h2`
  text-align: center;
`;

const CardDetails = styled.div`
  width: 60%;
  min-width: 350px;
  border: 1px solid #ccc;
  background: #eee;
  border-radius: 15px;
  padding: 10px 20px;
`;

const RowData = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  direction: rtl;
  text-align: right;
  margin-bottom: 10px;
  margin-top: 20px;
  div[name="des"] {
    display: block;
    width: 100%;
  }
`;

const RawRadio = styled.div`
  width: 100%;
  direction: rtl;
`;

const Label = styled.div`
  font-size: 15px;
  font-weight: 600;
`;

const RadioCon = styled.div`
  width: 50%;
  height: 40px;
  line-height: 40px;
  margin-top: 20px;
  display: block;
  float: right;
  label {
    float: right;
  }
  input {
    width: 10px !important;
    height: 10px !important;
    margin-right: 10px;
    margin-top: 13px;
    cursor: pointer;
  }
  inout:focus {
    border-bottom: 1px solid #b05082;
  }
`;

const Value = styled.div`
  font-size: 20px;
  select {
    width: 200px;
    height: 40px;
    outline: medium none;
    border: medium none;
    margin-right: 30px;
  }
`;

const FixDown = styled.button`
  font-size: 16px;
  font-weight: 400;
  font-family: dana !important;

  width: 100%;
  height: 50px;
  text-decoration: none;
  border: none;

  position: fixed;
  bottom: 0;
  left: 0;
  background: #ff4177;
  color: #fff;

  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  box-shadow: 0 0 3px #333;
  transition: all 200ms;

  :hover {
    background: #994177;
  }
`;

const StyledForm = styled(Form)`
  width: 90%;
  max-width: 800px;
`;

const StyledField = styled(Field)`
  height: 40px;
  border-radius: 5px;
  width: 200px;
  border: 1px solid #ccc;
  padding: 5px 8px;

  font-size: 16px;
  font-weight: 300;
  font-family: dana !important;
`;
