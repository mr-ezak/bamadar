import React, { useEffect, useState, useCallback, useContext } from "react";
import styled from "styled-components";
import Head from "next/head";
import Router, { useRouter } from "next/router";

import { Header } from "../components/Header";
import { config, getQueryVariable } from "../utils/main";
import { Product } from "../components/Product";
import { Loading } from "../components/Loading";
import { Categories } from "../components/Categories";
import { LeftArrow } from "../components/icons/Icons";
import { SearchIcon } from "../components/icons/SearchIcon";
import { Pages } from "../components/shared/Pages";
import { CardContext } from "./_app";
import { MainWrapper, ContentWrapper } from ".";
// import { CardIcon } from "../components/icons/CardIcon";

export default () => {
  const [searchInput, setSearchInput] = useState("");
  const [products, setProducts] = useState([]);
  const [pageData, setPageData] = useState(undefined);

  const { items, addToCard, increaseCardBalance } = useContext(CardContext);

  const { query, push } = useRouter();
  const action = query.action;
  const page = query.page || undefined;
  const q = query.q || searchInput; // search name

  useEffect(() => {
    const type = action === "search" ? "byName" : "byCat";
    const name = action === "search" ? q : action;

    const isCancelled = { current: false };

    search(name, type, page, q).then(({ pageData, products }) => {
      if (isCancelled.current) {
        return;
      }
      if (pageData && products) {
        setPageData(pageData);
        setProducts(products);
      }
    });

    return () => {
      isCancelled.current = true;
    };
  }, [action, page, q, searchInput]);

  // add to basket

  return (
    <MainWrapper>
      <Head>
        <title>جستجو :: بامادر</title>
      </Head>
      <Header display={"none"} />
      <ContentWrapper>
        <Boxed>
          {products &&
            products.length != 0 &&
            products.map((item, i) => (
              <Product
                key={item.id}
                apiAdd={config}
                name={item.name}
                measurement={item.measurement}
                price={item.price}
                finalPrice={item.sell_price}
                image={item.images == null ? "empty" : item.images.thumb}
                addToCard={() => addToCard(item)}
                context={increaseCardBalance}
              />
            ))}
          {products.length != 0 && pageData && (
            <Pages
              maxPage={pageData.last_page}
              currentPage={page ? page : pageData.current_page}
            />
          )}

          {products.length == 0 && searchInput != "" && <Loading />}
          {products.length == 0 && searchInput == "" && (
            <>
              <CatTitle>دسته بندی ها</CatTitle>
              <Categories apiAdd={config} />
            </>
          )}
        </Boxed>
      </ContentWrapper>
    </MainWrapper>
  );
};

async function search(name, type, page, q) {
  if (type == "byName") {
    if (name.length >= 2) {
      let url = config + `/api/get_products?name=${name}`;
      if (page) {
        url += "&page=" + page;
      }
      const request = await fetch(url);
      const productsJson = await request.json();
      return { pageData: productsJson.meta, products: productsJson.data };
    } else {
      return { pageData: undefined, products: undefined };
    }
  } else if (type == "byCat") {
    let url = config + `/api/get_products?category=${name}`;
    if (page) {
      url += "&page=" + page;
    }
    const request = await fetch(url);
    const productsJson = await request.json();

    return { pageData: productsJson.meta, products: productsJson.data };
  }
}

const CatTitle = styled.h2`
  width: 80%;
  height: 40px;
  border-bottom: 1px dashed #333;
  margin: 10px auto;
  font-size: 16px;
  font-weight: 400;
  color: #333;
  text-align: center;
`;

const Boxed = styled.div`
  padding: 20px;
`;
