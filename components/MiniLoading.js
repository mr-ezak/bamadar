import React, { Component } from "react";
import styled from "styled-components";
import { ReloadingIcon } from "./icons/Icons.js";

export const MiniLoading = () => {
  return (
    <LoadWrapper>
      {/* <LoadingLogo>
        <LoadingImg src='/images/Logo.png' />
        <LoadingPar align='center'>هایپر مادر</LoadingPar>
      </LoadingLogo> */}
      <LoadingWrapper>
        <ReloadingIcon />
        <span>در حال بارگزاری</span>
      </LoadingWrapper>
    </LoadWrapper>
  );
};

const LoadWrapper = styled.div`
  width: 100%;
  height: auto;
  display: block;
  margin: 50px auto;
`;
// const LoadingLogo = styled.div`
//   width: 150px;
//   height: 150px;
//   margin: 30px auto;
//   overflow: hidden;
// `;
// const LoadingImg = styled.img`
//   display: block;
//   width: 150px;
//   height: 150px;
// `;
// const LoadingPar = styled.p`
//   width: 100%;
//   height: 20px;
//   color: #b05082;
//   font-size: 20px;
//   text-align: center;
// `;
const LoadingWrapper = styled.div`
  width: 200px;
  height: 80px;
  margin: 10px auto;
  overflow: hidden;
  svg {
    width: 50px;
    height: 50px;
    display: block;
    margin: 5px auto;
  }
  span {
    width: 100%;
    display: block;
    font-size: 14px;
    text-align: center;
    color: #fff;
  }
`;
