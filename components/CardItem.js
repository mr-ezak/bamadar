import React from "react";
import styled from "styled-components";
import { AddToCard } from "./icons/AddToCard";
import { RemoveItem } from "./icons/RemoveItem";
import { Price } from "./shared/Price";

export const CardItem = ({
  name,
  balance,
  finalPrice,
  add,
  measurement,
  remove,
}) => {
  return (
    <ProductItem>
      <Right>
        <Name>{name}</Name>
        <Row>
          <SumPrice>
            مجموع: <Price>{finalPrice}</Price>
          </SumPrice>
        </Row>
        <Row>
          <SumPrice>
            واحد:{" "}
            {measurement != "گرم"
              ? measurement
              : balance >= 1
              ? "کیلوگرم"
              : measurement}
          </SumPrice>
        </Row>
      </Right>
      <Left>
        <BalanceControl>
          <ButtonWrapper onClick={() => add()}>
            <AddToCard />
          </ButtonWrapper>

          <Balance>
            {measurement == "گرم" && balance < 1 ? balance * 1000 : balance}
          </Balance>

          <ButtonWrapper onClick={() => remove()}>
            <RemoveItem />
          </ButtonWrapper>
        </BalanceControl>
      </Left>
    </ProductItem>
  );
};

const ProductItem = styled.div`
  width: 100%;
  min-height: 50px;
  border-radius: 15px;
  border-bottom: 1px solid #999;
  padding: 10px;
  margin-bottom: 10px;

  display: flex;
  direction: rtl;
  justify-content: space-between;
  align-items: center;

  cursor: pointer;
  :hover {
    background: #eee;
  }
`;
const Right = styled.div``;
const Left = styled.div``;

const Name = styled.div`
  padding: 0 5px;
`;

const ButtonWrapper = styled.div`
  width: 35px;
  height: 35px;

  background: #999;
  border-radius: 5px;
  padding: 5px;

  path {
    fill: #fff;
  }

  :active {
    background: #de7119;
  }
`;

const Row = styled.div`
  margin-top: 10px;
  display: flex;
`;

const SumPrice = styled.div`
  div {
    display: inline-flex;
  }
`;
const Balance = styled.div`
  margin: 0 5px;
  font-size: 20px;
`;

const BalanceControl = styled.div`
  display: flex;
  align-items: center;
`;
