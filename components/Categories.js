import React, { useState, useEffect, useCallback } from "react";
import styled, { css } from "styled-components";
import * as Cats from "./icons/Cats";
import Router from "next/router";
import axios from "axios";
import { Loading } from "./Loading";

export const Categories = ({ apiAdd, setSubPage, setSubCats }) => {
  const [cats, setCats] = useState([]);
  //   const [catsHeight, setCatsHeight] = useState("330px");
  //   const [btnText, setBtnText] = useState("بیشتر");

  const getCats = useCallback(async () => {
    const response = await axios.get(apiAdd + "/api/main_categories");
    const arr = response.data.data;
    setCats(arr);
  }, [apiAdd]);

  useEffect(() => {
    getCats();
  }, [getCats]);

  return (
    <Wrapper>
      <CatsCon>
        {cats.length != 0 ? (
          cats.map((data, index) => (
            <Item
              key={index}
              onClick={() => {
                Router.push("/subCategories?id=" + data.id);
              }}
            >
              {data.image == null ? (
                <Cats.NutsIcon />
              ) : (
                <img src={apiAdd + data.image} />
              )}
              <span>{data.name}</span>
            </Item>
          ))
        ) : (
          <div>درحال بارگزاری</div>
          //   <Loading />
        )}
      </CatsCon>
      {/* <Button
        onClick={() => {
          changeCatsStat();
        }}
      >
        {btnText}
      </Button> */}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  direction: rtl;
`;
// const Sldr = styled.div`
//   width: 100%;
//   img {
//     width: 100%;
//   }
// `;
const CatsCon = styled.div`
  width: 100%;
  height: auto;
  overflow: hidden;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  transition: all 500ms;
`;
const Item = styled.div`
  width: 100px;
  height: 150px;
  border-radius: 5px;
  background: white;
  margin-top: 10px;
  margin-right: 10px;
  cursor: pointer;
  /* outline: 1px dashed #333; */
  outline-offset: -8px;
  -moz-outline-radius: 5px;
  padding: 10px;

  transition: all 300ms;
  box-shadow: 0 0 10px #ccc;

  svg {
    display: block;
    width: 50px;
    height: 50px;
    margin: 5px auto;
  }
  img {
    display: block;
    width: 90px;
    height: 100px;
    margin: 2px auto;
  }
  span {
    display: block;
    color: #000;
    font-size: 12px;
    text-align: center;
  }
  :hover {
    background: #955f7b;
  }
`;
// const Button = styled.div`
//   width: 100px;
//   height: 30px;
//   line-height: 30px;
//   text-align: center;
//   display: block;
//   margin: 15px auto;
//   border-radius: 5px;
//   border: 1px dashed #3498db;
//   color: #3498db;
//   cursor: pointer;
// `;
