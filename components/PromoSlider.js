import { useState, useEffect, useCallback, useContext } from "react";
import styled from "styled-components";
import { config } from "../utils/main";
import Carousel from "react-multi-carousel";
import { AddToCartIcon, ContactIcon } from "./icons/Icons";
import { MiniLoading } from "./MiniLoading";
import { Price } from "./shared/Price";
import { CardContext } from "../pages/_app";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 4,
    slidesToSlide: 1, // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 4,
    slidesToSlide: 1, // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 4,
    slidesToSlide: 1, // optional, default to 1.
  },
};
export const PromoSlider = ({ msgBox, setMsgBox }) => {
  const [proPros, setProPros] = useState([]);

  const { addToCard } = useContext(CardContext);

  const getProPros = useCallback(async () => {
    const request = await fetch(config + `/api/get_promotions`);
    const productsJson = await request.json();
    setProPros(productsJson.data);
  }, [setProPros]);

  useEffect(() => {
    getProPros();
  }, [getProPros]);

  return (
    <Container>
      <Carousel
        additionalTransfrom={0}
        arrows={false}
        autoPlaySpeed={3000}
        centerMode={false}
        className=""
        containerClass="container"
        // dotListClass=''
        draggable={true}
        focusOnSelect={false}
        infinite
        itemClass=""
        keyBoardControl
        minimumTouchDrag={80}
        renderButtonGroupOutside={false}
        rtl={true}
        // renderDotsOutside
        responsive={{
          desktop: {
            breakpoint: {
              max: 3000,
              min: 1024,
            },
            items: 4,
          },
          mobile: {
            breakpoint: {
              max: 464,
              min: 0,
            },
            items: 3,
          },
          tablet: {
            breakpoint: {
              max: 1024,
              min: 464,
            },
            items: 4,
          },
        }}
        showDots={false}
        sliderClass=""
      >
        {proPros &&
          proPros.length != 0 &&
          proPros.map((data, index) => (
            <ItemCon key={index} discount={data.discount}>
              <Item
                src={data.images.thumb != null && config + data.images.thumb}
              />
              <ItemText>{data.name}</ItemText>
              <Price cross>{data.price}</Price>
              <Price>{data.sell_price}</Price>
              <ItemBtn
                onClick={() => {
                  addToCard(data);
                }}
              >
                <AddToCartIcon />
              </ItemBtn>
            </ItemCon>
          ))}
      </Carousel>
      {proPros.length == 0 && <MiniLoading />}
    </Container>
  );
};
const Container = styled.div`
  position: relative;
  li {
    @media only screen and (max-width: 600px) {
      margin: 25px;
      margin-top: 0;
    }
  }
`;

const ItemCon = styled.div`
  width: 150px;
  height: 250px;
  border-radius: 5px;
  position: relative;

  margin-left: 50px ;
  background: white;
  /* outline: 1px dashed #666; */

  outline-offset: -5px;
  -moz-outline-radius: 5px;
  /* overflow: hidden; */
  text-align: center;
  direction: rtl;

  &::before {
    content: "${(props) => (props.discount ? props.discount + "%" : null)}";
    position: absolute;
    left: 0;
    top: 0;
    padding: 2.5px 5px;
    background-color: #fb3449;
    border-radius: 15px;
    transform: translateX(-50%);
    color: white;
    font-size: 1rem;
    font-weight: bolder;
  }
  display: flex;
  flex-direction: column;
  align-items: center;

  span {
    display: block;
    width: 90%;
    margin: 0px auto;
    font-size: 12px;
    text-align: center;
    color: #000;
  }
`;
const Item = styled.img`
  width: 80px;
  height: 80px;
  display: block;
  border-radius: 50px;
  margin-top: 8px;
`;
const ItemText = styled.span`
  width: 100%;
  color: #efefef;
  display: block;
  text-align: center;
`;
const ItemBtn = styled.div`
  width: 45px;
  height: 45px;
  padding: 5px;
  border-radius: 5px;
  border: 1px dashed #666;
  cursor: pointer;
  transition: all 300ms;
  position: absolute;
  margin-left: auto;
  margin-right: auto;
  left: 0;
  right: 0;
  bottom: 10px;
  :hover {
    background: #7b385b;
  }
`;
