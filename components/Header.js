import React, { useEffect, useState, useContext, useRef } from "react";
import styled from "styled-components";
import Router, { useRouter } from "next/router";

import { config } from "../utils/main";
import { SearchIcon } from "./icons/SearchIcon";
import { CardIcon } from "./icons/CardIcon";
import { LeftArrow } from "../components/icons/Icons";
import { GoBackIcon } from "./icons/Icons";
import { CardContext } from "../pages/_app";

export const Header = (display) => {
  const [searchInput, setSearchInput] = useState("");
  const inputRef = useRef();
  const { query, push, pathname } = useRouter();
  const q = query.q || "";

  useEffect(() => {
    setSearchInput(q);
    if (pathname == "/products") {
      inputRef.current.focus();
    }
  }, [pathname, q]);

  // User's Card(basket)
  const { items, addToCard } = useContext(CardContext);

  return (
    <Wrapper>
      <a
        onClick={() => {
          Router.push("/");
        }}
        style={{ textDecoration: "none", cursor: "pointer" }}
      >
        <Title>بامادر</Title>
      </a>

      <GoBackCon
        onClick={() => {
          Router.push("/");
        }}
        display={display}
      >
        {/* <span>بازگشت</span> */}
        <GoBack>
          <LeftArrow />
        </GoBack>
      </GoBackCon>
      <HeaderBody>
        <Search>
          <IconWrapper>
            <SearchIcon />
          </IconWrapper>
          <Input
            ref={inputRef}
            placeholder="جستجو نام محصول"
            value={searchInput}
            onChange={(e) => {
              const value = e.currentTarget.value;
              setSearchInput(value);
              if (value.length > 2) {
                push({
                  pathname: "/products",
                  query: { ...query, action: "search", q: value },
                });
              }
            }}
          ></Input>
        </Search>
        <Card onClick={() => Router.push("/cart")}>
          <CardCount>{items.length ? items.length : 0}</CardCount>
          <CardIconWrapper>
            <CardIcon />
          </CardIconWrapper>
        </Card>
      </HeaderBody>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  width: 100%;
  height: 150px;
  padding: 10px 10px;
  padding-bottom: 60px;
  direction: rtl;
  background: #fff;
`;

const Title = styled.h3`
  width: 40%;
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 30px;
  text-align: center;
  margin: 0px auto;
  color: #b05082;
`;

const GoBackCon = styled.div`
  width: 90px;
  height: 40px;
  position: absolute;
  top: 0px;
  left: 10px;
  display: ${(p) => (p.display ? p.display : "none")};
  cursor: pointer;
  span {
    display: block;
    float: right;
  }
`;

const GoBack = styled.div`
  width: 30px;
  height: 40px;
  float: left;
  overflow: hidden;
  margin-top: 10px;

  path {
    fill: #b05082;
  }
`;

const HeaderBody = styled.div`
  width: 100%;
  margin-top: 20px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  direction: rtl;
`;

const Search = styled.div`
  flex-grow: 1;
  width: 284px;
  height: 50px;
  margin-left: 10px;
  padding: 0 5px;

  background: #ffffff;
  border-radius: 4px;

  display: flex;
  justify-content: space-between;
  align-items: center;
  overflow: hidden;
`;

const Input = styled.input`
  flex-grow: 1;
  display: inline-block;
  height: 100%;
  padding: 3px 10px;
  padding-right: 50px;

  outline: none;
  border: none;
  background: #f0f0f1;
  border-radius: 15px;

  font-size: 18px;
  font-family: dana !important;
`;

const IconWrapper = styled.div`
  flex-shrink: 0;
  width: 30px;
  height: 30px;
  opacity: 0.4;
  transform: translateX(calc(-100% - 15px));
  path {
    width: 100%;
  }
`;

const Card = styled.div`
  flex-shrink: 0;
  width: 50px;
  height: 50px;
  /* border: 1px dashed #efefef;
  background: #b05082; */

  display: flex;
  justify-content: center;
  align-items: center;

  position: relative;
  cursor: pointer;

  :hover {
    background: #7b385b;
  }
`;
const CardIconWrapper = styled.div`
  width: 35px;
  height: 35px;

  path {
    width: 100%;
    fill: #a6a6a8;
  }
`;

const CardCount = styled.div`
  position: absolute;
  top: -5px;
  right: -7px;
  width: 18px;
  height: 18px;
  line-height: 22px;
  border-radius: 50%;
  background: #ff0936;
  text-align: center;
  font-size: 17px;
  color: #fff;
`;

// const fixFarsi = text => {
//   return text.replace(/ی/gi, 'ي')
// }
