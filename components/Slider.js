import styled from "styled-components";
import Carousel from "react-multi-carousel";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
    slidesToSlide: 3 // optional, default to 1.
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
    slidesToSlide: 2 // optional, default to 1.
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1 // optional, default to 1.
  }
};
export const MySlider = () => {
  return (
    <Carousel
      additionalTransfrom={0}
      arrows={false}
      autoPlaySpeed={3000}
      centerMode={false}
      showDots={false}
      className=''
      containerClass='container'
      // dotListClass=''
      draggable={true}
      focusOnSelect={false}
      infinite
      itemClass=''
      keyBoardControl
      minimumTouchDrag={80}
      renderButtonGroupOutside={false}
      // renderDotsOutside
      responsive={{
        desktop: {
          breakpoint: {
            max: 3000,
            min: 1024
          },
          items: 1
        },
        mobile: {
          breakpoint: {
            max: 464,
            min: 0
          },
          items: 1
        },
        tablet: {
          breakpoint: {
            max: 1024,
            min: 464
          },
          items: 1
        }
      }}
      showDots
      sliderClass=''>
      <ItemCon>
        <Item src='/images/slider/1.jpeg' />
      </ItemCon>
      <ItemCon>
        <Item src='/images/slider/2.jpeg' />
      </ItemCon>
      <ItemCon>
        <Item src='/images/slider/3.jpeg' />
      </ItemCon>
      <ItemCon>
        <Item src='/images/slider/4.jpeg' />
      </ItemCon>
    </Carousel>
  );
};

const ItemCon = styled.div`
  width: 100%;
  height: auto;
  //   @media (max-width: 1000px) {
  //     img {
  //       max-height: 400px;
  //     }
  //   }
  //   @media (max-width: 500px) {
  //     img {
  //       max-height: 300px;
  //     }
  //   }
  //   @media (max-width: 360px) {
  //     img {
  //       max-height: 200px;
  //     }
  //   }
`;
const Item = styled.img`
  width: 100%;
  height: 100%;
  //   height: 500px;
`;
