import React, { useState, useEffect, useCallback } from "react";
import styled, { css } from "styled-components";
import axios from "axios";
import { PhoneIcon } from "./icons/Icons";
import { config } from "../utils/main";

export const Login = ({ logged, setLogged }) => {
  const [mobile, setMobile] = useState(undefined);
  const [otp, setOtp] = useState(undefined);
  const [step, setStep] = useState("mobile"); //mobile | verify

  //TODO -> should handle login
  const login = useCallback(async () => {
    const response = await axios.post(config + "/api/user/login", {
      mobile: mobile,
    });
    if (response.data.message == "EnterOTP") {
      setMobile(mobile);
      setStep("verify");
    }
  }, [mobile]);

  //TODO -> should handle login
  const checkOtp = useCallback(async () => {
    const res = await axios.post(config + "/api/user/verify", {
      mobile: mobile,
      otp: otp,
    });

    if (({ res }, res.data.data.access_token)) {
      const token = ({ res }, res.data.data.access_token);
      window.localStorage.setItem("MadarLogged", 1);
      window.localStorage.setItem("access_token", token);
      window.localStorage.setItem("MadarCell", mobile);
      setLogged("1");
    }
  }, [mobile, otp, setLogged]);

  return (
    <Wrapper>
      {step == "mobile" && (
        <>
          <TextTitle>ورود کاربران</TextTitle>
          <Title>
            <PhoneIcon />
          </Title>
          <Text>لطفا برای دریافت کد تاییدیه شماره همراه خود را وارد کنید</Text>
          <Field
            placeholder='۰۹....'
            onChange={(event) => {
              setMobile(event.currentTarget.value);
            }}
          />
          <Button onClick={() => login()}>دریافت کد</Button>
        </>
      )}

      {step == "verify" && (
        <>
          <TextTitle>ورود کاربران</TextTitle>
          <Title>
            <PhoneIcon />
          </Title>
          <Text>کد ارسال شده به موبایل خود را وارد کنید:</Text>
          <Field
            onChange={(event) => {
              setOtp(event.currentTarget.value);
            }}
          />
          <Button onClick={() => checkOtp()}>تایید</Button>
        </>
      )}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  width: 80%;
  margin: 0px auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  direction: rtl;
`;

const TextTitle = styled.span`
  display: block;
  width: 100%;
  color: #000;
  height: 40px;
  border-bottom: 1px dashed #333;
  margin: 20px auto;
  text-align: center;
`;
const Title = styled.div`
  width: 130px;
  height: 130px;
  border-radius: 65px;
  margin: 30px auto;
  outline: 1px dashed #ccc;
  outline-offset: -10px;
  -moz-outline-radius: 15px;
  overflow: hidden;
  svg {
    display: block;
    margin: 30px auto;
  }
`;

const Text = styled.h2`
  text-align: center;
  font-size: 13px;
  font-weight: 400;
  color: #666;
`;

const Field = styled.input`
  height: 40px;
  border-radius: 5px;
  direction: ltr;
  width: 200px;
  border: 1px solid #ccc;
  padding: 5px 8px;
  margin-top: 10px;
  font-size: 16px;
  font-weight: 300;
  font-family: dana !important;
`;

const Button = styled.div`
  display: inline-flex;
  width: auto;
  padding: 8px 10px;
  margin-top: 10px;
  border-radius: 5px;
  border: 1px solid #27ae60;
  background: #fff;
  color: #27ae60;
  cursor: pointer;
`;
