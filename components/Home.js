import React, { useState, useEffect } from "react";
import styled, { css } from "styled-components";
import { GroceryIcon } from "./icons/Icons";
import { MySlider } from "./Slider";
import { PromoSlider } from "./PromoSlider";
import { Categories } from "./Categories";
// import { SubCategories } from "./SubCategories";

export const Home = ({ msgBox, setMsgBox, apiAdd }) => {
  const [subPage, setSubPage] = useState("categories");
  const [subCats, setSubCats] = useState([]);
  return (
    <Wrapper>
      <Sldr>
        {/* <img src='slider1.jpeg' /> */}
        <MySlider />
      </Sldr>
      <PromoCon>
        <PromoTitle>فروش ویژه</PromoTitle>
        <PromoSlider msgBox={msgBox} setMsgBox={setMsgBox} />
      </PromoCon>
      <CatCon>
        <CatTitle>دسته بندی</CatTitle>

        {subPage == "categories" && (
          <Categories
            apiAdd={apiAdd}
            setSubPage={setSubPage}
            setSubCats={setSubCats}
          />
        )}

        {/* {subPage == "subCategories" && (
          <SubCategories
            setPage={setPage}
            setSubPage={setSubPage}
            subCats={subCats}
            setCatId={setCatId}
          />
        )} */}
      </CatCon>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  background-color: #f2f2f2;
`;

const Sldr = styled.div`
  width: 100%;
  img {
    width: 100%;
  }
`;

const PromoCon = styled.div`
  width: 100%;
  height: 350px;
  background-image: url("http://s10.picofile.com/file/8393526984/emad.png");
  margin-top: 50px;
  border-radius: 25px;
`;
const PromoTitle = styled.p`
  width: 130px;
  border-radius: 50px;
  height: 40px;
  line-height: 40px;
  display: block;
  text-align: center;
  margin: 0px auto;
  margin-top: -10px;
  margin-bottom: 20px;
  color: white;
  background-color: #f655a3;
  /* border-bottom: 1px dashed #efefef; */
`;

const CatCon = styled.div`
  width: 100%;
  height: auto;
  padding: 10px;
`;
const CatTitle = styled.p`
  width: 90%;
  height: 40px;
  line-height: 40px;
  display: block;
  text-align: center;
  margin: 5px auto;
  margin-bottom: 20px;
  color: #333;
  border-bottom: 1px dashed #333;
`;
const CategoriesCon = styled.div`
  width: 55%;
  height: 70px;
  background: #884b6b;
  margin: 0px auto;
  margin-bottom: 10px;
  border-radius: 5px;
  outline: 1px dashed #efefef;
  outline-offset: -8px;
  -moz-outline-radius: 5px;
  overflow: hidden;
  cursor: pointer;
  svg {
    display: block;
    float: right;
    margin-top: 17px;
    margin-right: 10px;
    width: 45px;
    height: 45px;
  }
  span {
    float: right;
    display: block;
    margin-right: 10px;
    margin-top: 20px;
  }
`;

const Item = styled.div`
  width: 100%;
  background: #884b6b;
  border-radius: 5px;
  padding: 15px;
  outline: 1px dashed #efefef;
  outline-offset: -5px;
  -moz-outline-radius: 5px;
  text-align: center;
  margin-bottom: 10px;
  cursor: pointer;
  text-decoration: line-through;
`;
