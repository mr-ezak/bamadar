import React, { useState, useEffect, useCallback } from "react";
import styled, { css } from "styled-components";
import axios from "axios";

export const Dummy = ({ setPage }) => {
  const Cats = {
    data: [
      {
        id: 9,
        parent_id: null,
        name: "آجیل و خشکبار فله",
        slug: null
      },
      {
        id: 39,
        parent_id: null,
        name: "آرایشی و بهداشتی",
        slug: null
      },
      {
        id: 18,
        parent_id: null,
        name: "پلاسکو",
        slug: null
      },
      {
        id: 12,
        parent_id: null,
        name: "ترشی و لبنیات فله",
        slug: null
      },
      {
        id: 44,
        parent_id: null,
        name: "تنقلات بسته بندی",
        slug: null
      },
      {
        id: 87,
        parent_id: null,
        name: "چاشنی و افزودنی بسته بندی",
        slug: null
      },
      {
        id: 5,
        parent_id: null,
        name: "دستمال و شوینده",
        slug: null
      },
      {
        id: 17,
        parent_id: null,
        name: "دسر",
        slug: null
      },
      {
        id: 8,
        parent_id: null,
        name: "عطاری",
        slug: null
      },
      {
        id: 7,
        parent_id: null,
        name: "غذای گرم",
        slug: null
      },
      {
        id: 4,
        parent_id: null,
        name: "کالاهای اساسی و خوارو بار",
        slug: null
      },
      {
        id: 54,
        parent_id: null,
        name: "کنسرو و غذای آماده",
        slug: null
      },
      {
        id: 21,
        parent_id: null,
        name: "لبنیات بسته بندی",
        slug: null
      },
      {
        id: 16,
        parent_id: null,
        name: "لوازم آشپزخانه",
        slug: null
      },
      {
        id: 97,
        parent_id: null,
        name: "مادر و کودک",
        slug: null
      },
      {
        id: 111,
        parent_id: null,
        name: "محصولات صبحانه",
        slug: null
      },
      {
        id: 11,
        parent_id: null,
        name: "مواد پروتئینی",
        slug: null
      },
      {
        id: 79,
        parent_id: null,
        name: "مواد غذایی منجمد",
        slug: null
      },
      {
        id: 13,
        parent_id: null,
        name: "میوه،سبزی و صیفی جات",
        slug: null
      },
      {
        id: 20,
        parent_id: null,
        name: "نان و شیرینی",
        slug: null
      },
      {
        id: 10,
        parent_id: null,
        name: "نوشیدنی",
        slug: null
      },
      {
        id: 14,
        parent_id: null,
        name: "یکبار مصرف",
        slug: null
      }
    ],
    status: "ok"
  };

  return (
    <Wrapper>
      <>
        <Title>
          <img src='../images/Soon.png' />
        </Title>
        {/* <Button onClick={() => setPage("home")}>خانه</Button> */}
        <Text>به زودی با شما خواهیم بود</Text>
      </>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  direction: rtl;
`;

const Title = styled.h2`
  font-size: 16px;
  font-weight: 400;
  color: #333;
  text-align: center;
  margin-top: 50px;
  img {
    display: block;
    width: 200px;
    height: 220px;
    margin: 0px auto;
  }
`;
const Text = styled.div`
  width: 300px;
  height: 50px;
  line-height: 50px;
  color: #b05082;
  font-weight: bold;
  margin: 20px auto;
  font-size: 18px;
  text-align: center;
`;
const Button = styled.div`
  width: 150px;
  text-align: center;
  display: block;
  padding: 8px 10px;
  margin: 10px auto;
  border-radius: 5px;
  background: #3498db;
  color: #efefef;
  cursor: pointer;
`;
