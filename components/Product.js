import React from "react";
import styled from "styled-components";
import { AddToCard } from "./icons/AddToCard";
import { Price } from "./shared/Price";

export const Product = ({
  apiAdd,
  name,
  measurement,
  price,
  finalPrice,
  image,
  addToCard,
}) => {
  //   console.log(image);
  return (
    <ProductItem>
      <Pic>
        <img src={image == "empty" ? "/images/Logo.png" : apiAdd + image} />
      </Pic>
      <Right>
        <Name>{name}</Name>
        <Measure>واحد: {measurement}</Measure>
        <Row>
          {price != finalPrice && (
            <>
              <Price cross>{price}</Price>
              <Price>{finalPrice}</Price>
            </>
          )}
          {price == finalPrice && <Price>{finalPrice}</Price>}
        </Row>
      </Right>
      <Left>
        <AddToCardButton onClick={() => addToCard()}>
          <AddToCard />
        </AddToCardButton>
      </Left>
    </ProductItem>
  );
};

const ProductItem = styled.div`
  width: 100%;
  min-height: 50px;
  border-radius: 15px;
  border-bottom: 1px solid #999;
  padding: 10px;
  margin-bottom: 10px;

  display: flex;
  direction: rtl;
  justify-content: space-between;
  align-items: center;
  color: #000;

  cursor: pointer;
  :hover {
    background: #eee;
  }
`;
const Pic = styled.div`
  float: right;
  width: 80px;
  height: 80px;
  border-radius: 10px;
  img {
    display: block;
    width: 100%;
    height: 100%;
    border-radius: 10px;
  }
`;
const Right = styled.div`
  flex-grow: 1;
  padding-right: 20px;
`;
const Left = styled.div``;

const Name = styled.div`
  padding: 0 5px;
`;

const Measure = styled.div`
  padding: 0 5px;
`;

const AddToCardButton = styled.div`
  width: 35px;
  height: 35px;

  background: #999;
  border-radius: 5px;
  padding: 5px;

  path {
    fill: #fff;
  }

  :active {
    background: #de7119;
  }
`;

const Row = styled.div`
  margin-top: 10px;
  display: flex;
  text-align: center;

  color: #000;
`;
