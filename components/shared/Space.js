import React from 'react'

export const Space = ({ horizontal, vertical }) => (
  <div style={{ width: horizontal, height: vertical }} />
)
