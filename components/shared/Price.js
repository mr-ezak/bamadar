import React from "react";
import styled, { css } from "styled-components";

export const Price = ({ children, cross }) => {
  return <Wrapper cross={cross}>{numberWithCommas(children)} ریال</Wrapper>;
};

const numberWithCommas = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

const Wrapper = styled.div`
  font-weight: 600;
  padding: 0 5px;
  //   margin-right: 20px;

  ${(p) =>
    p.cross &&
    css`
      text-decoration: line-through;
      opacity: 0.7;
    `}
`;
