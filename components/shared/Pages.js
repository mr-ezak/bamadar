import React, { useEffect, useState, useCallback } from "react";
import styled, { css } from "styled-components";
import Link from "next/link";
import { useRouter } from "next/router";

export const Pages = ({ maxPage, currentPage }) => {
  const { query, push, pathname } = useRouter();
  console.log({ query });
  return (
    <PageWrapper>
      {[...Array(maxPage)].map((v, i) => {
        if (i >= 50) return null;
        const pageNumber = i + 1;
        // const pageAddress = `${baseUrl}&page=${pageNumber}`;
        return (
          <Page
            key={"product" + i}
            onClick={() => {
              push({
                pathname,
                query: { ...query, page: pageNumber },
              });
            }}
            selected={pageNumber == currentPage}
          >
            {pageNumber}
          </Page>
        );
      })}
    </PageWrapper>
  );
};

const PageWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  direction: rtl;

  align-content: center;
  align-items: center;

  margin: 20px 0 30px;
`;

const Page = styled.div`
  width: 50px;
  height: 50px;
  border-radius: 5px;
  border: 1px solid #eee;
  text-align: center;
  margin-left: 5px;
  line-height: 50px;
  cursor: pointer;

  ${(p) =>
    p.selected &&
    css`
      background-color: #00bfd6;
      border: 1px solid #00bfd6;
      color: #fff;
    `}
`;
