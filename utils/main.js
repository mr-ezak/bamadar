export const config = "https://my.bamadar.com";

export const loginCheck = () => {
  if (window.localStorage.getItem("MadarLogged")) {
    return window.localStorage.getItem("MadarLogged");
  }
};

export const getQueryVariable = (variable) => {
  if (typeof window != "object") return undefined;
  let query = window.location.search.slice(1);
  let vars = query.split("&");
  for (const element of vars) {
    let pair = element.split("=");
    if (pair[0] == variable) {
      return pair[1];
    }
  }
  return undefined; //not found
};
